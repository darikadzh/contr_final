class RatingsController < InheritedResources::Base

  def create
    place = Place.find(params[:place_id])
    rating = place.ratings.new rating_params
    rating.user = current_user
    rating.save
    redirect_to place_path(place)
  end

  private
  
    def rating_params
      params.require(:rating).permit(:text, :food_score, :service_score, :interior_score)
    end
end

