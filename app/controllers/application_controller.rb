class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
   before_action :configure_permitted_parameters, if: :devise_controller?
  
  private
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  def access_denied(exception)
    flash[:danger] = exception.message
    redirect_to root_url
  end

  def ensure_admin!
    unless user_signed_in?
      redirect_to root_path

      return false
    end
  end
end
