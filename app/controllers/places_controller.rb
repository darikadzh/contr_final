class PlacesController < InheritedResources::Base
before_action :set_place, only: [:show, :edit, :update, :destroy]
before_action :ensure_admin!, only: [:new, :create]

	def index
		@categories = Category.all
    if params[:category]
    	@places = Place.where(:category => params[:category], active: true)
  	else
    	@places = Place.where(active: true)
    end
  end

  def show
  	@overall = Place.average_score
    @food_average = @place.reviews.average(:food)
    @food_average = @food_average.round(1) if @food_average
    @service_average = @place.reviews.average(:service)
    @service_average = @service_average.round(1) if @service_average
    @interior_average = @place.reviews.average(:interior)
    @interior_average = @interior_average.round(1) if @interior_average
    
    all_average = [@food_average, @service_average, @interior_average]
    if all_average == [nil, nil, nil]
      @overall_average = 0
    else
      @overall_average = (all_average.sum / all_average.size.to_f).round(1) if all_average
    end
  end

  def new
    @place = Place.new
  end

  def edit
  end

  def create
    @place = Place.new(place_params)

    respond_to do |format|
      if @place.save
        format.html { redirect_to root_path, notice: 'Place was successfully created.' }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
  	def set_place
      @place = Place.find(params[:id])
    end

    def place_params
      params.require(:place).permit(:name, :description, :active, :user_id, :category_id)
    end
end

