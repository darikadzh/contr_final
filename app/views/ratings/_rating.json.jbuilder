json.extract! rating, :id, :text, :food_score, :service_score, :interior_score, :created_at, :updated_at
json.url rating_url(rating, format: :json)
