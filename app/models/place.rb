class Place < ApplicationRecord
	has_many :ratings
	belongs_to :user
	belongs_to :category
	validates :name, presence: true,
            uniqueness: true,
            length: {minimum: 3}
	validates :description, presence: true,
            length: {maximum: 5400}
end
