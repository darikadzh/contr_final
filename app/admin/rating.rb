ActiveAdmin.register Rating do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
index do
  selectable_column
  column :text
  column :food_score
  column :service_score
  column :interior_score
  actions
end

show do
  attributes_table do
  row :text
  row :food_score
  row :service_score
  row :interior_score
  end
end

end
