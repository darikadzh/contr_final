Rails.application.routes.draw do
	root 'places#index'
  resources :categories
  resources :places do 
    resources :ratings
  end
  ActiveAdmin.routes(self)
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
