class AddPlaceToRatings < ActiveRecord::Migration[5.1]
  def change
    add_reference :ratings, :place, foreign_key: true
  end
end
