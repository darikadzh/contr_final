class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.text :text
      t.integer :food_score
      t.integer :service_score
      t.integer :interior_score

      t.timestamps
    end
  end
end
