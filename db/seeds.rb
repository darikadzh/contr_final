# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
category1 = Category.create!(name: 'Cafe')
category2 = Category.create!(name: 'Pub')
category3 = Category.create!(name: 'Restaurant')

admin = User.create!(name:'admin', email:'admin@example.com', password: 'adminadmin', password_confirmation:'adminadmin', admin:true)
user = User.create!(name:'user', email:'user@example.com',password:'123456',password_confirmation:'123456')

place1 = Place.create!(name: 'Winchester', description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt incidunt, cum ullam architecto? Laudantium dolor necessitatibus excepturi tenetur ullam quibusdam veritatis, culpa debitis rem commodi, at obcaecati expedita laborum minima.", category: category3, user:user)
place1 = Place.create!(name: 'Afterlife', description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt incidunt, cum ullam architecto? Laudantium dolor necessitatibus excepturi tenetur ullam quibusdam veritatis, culpa debitis rem commodi, at obcaecati expedita laborum minima.", category: category2, user:admin)
place1 = Place.create!(name: 'Winchester', description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt incidunt, cum ullam architecto? Laudantium dolor necessitatibus excepturi tenetur ullam quibusdam veritatis, culpa debitis rem commodi, at obcaecati expedita laborum minima.", category:category1, user:user)